import json
import requests
FILE_NAME = 'myfile2.json'
alert_error_list = []

try:
    with open(FILE_NAME, 'r+') as file_read:
        json_data = json.load(file_read)
    hits = json_data['hits']['hits']
except FileNotFoundError:
    print(FILE_NAME+" not found...")
hits = hits[3000:4000]
count = 3001
available_count = 0
imported_count = 0
for hit in hits:
    alert_id = hit.get('_id',{})
    source = hit.get('_source',{})
    # print(count ,": " ,alert_id)
    # count += 1
    
    print(count ,": " ,alert_id)
    count += 1
    url = 'http://10.0.8.22:9200/' + '_xpack/watcher/watch/' + alert_id 
    source = json.dumps(source)
    # print(record)
    headers = {'Content-Type': 'application/json'}
    try:
        x = requests.put(url, data = source, headers = headers)
    except requests.ConnectionError as e: 
        print(e)
        print("Error occured while importing data. Alert. ID: {0}".format(alert_id))
    if(x.status_code == 201):
        imported_count += 1
    elif(x.status_code == 200):
        available_count += 1
    else:
        print(x.json())
        print("Error occured while importing data. Alert ID: {0} and status code {1}".format(alert_id, x.status_code))
