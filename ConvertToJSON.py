import json
import re

# Alerts metadata JSON file
FILE_NAME = 'Dummy_Alerts.json'
try:
#    with open(FILE_NAME, 'r+') as file_read:
#        json_data = json.load(file_read)
    file2 = open(FILE_NAME, errors='ignore')
    json_data = file2.read()
    jdata3 = eval(json_data)
    jdata = json.dumps(jdata3)
#    jdata1 = json.loads(jdata)
#    jdata1 = json.loads(json_data)

# File in correct JSON will be written
    file1 = open("Dummy_Alerts_valid.json","w")
    file1.write(jdata)
except FileNotFoundError:
    print(FILE_NAME+" not found...")