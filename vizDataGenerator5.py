# File used for generate random data for index-pattern based on related fields

# Standard library imports
import json
import random
import sys
import string
import datetime
import requests
import re
from dateutil import parser
import collections.abc
import time
from selenium import webdriver

time_mapper = {'s':1,'m':60, 'H':3600, 'h':3600, 'd':86400, 'w':604800, 'M':2592000, 'y':31536000}

def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            if not isinstance(d, dict):
                d = {}
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d

def is_digit(n):
    try:
        int(n)
        return True
    except ValueError:
        return  False

def index_fields(index):
    """Returns fields based on index from index-pattern data.

    :param index: index-pattern title for search in file
    :return: String of fields of given index
    """

    for hit in hits:
        if(hit['_id'] == 'index-pattern:' + index):
            title = hit['_source']['index-pattern']['title']
            title = title.replace('*', '1')
            # print('TITLE')
            # print(title)
            fields = hit['_source']['index-pattern']['fields']
            return title, fields

def get_random_string(length):
    """Return a random string of given length.

    :param length: Length of string for generation
    :return: String of random character with given length
    """

    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

def verify():
    """Start of the code."""
    URL = "http://10.0.8.22:5601/app/kibana#/visualize/edit/"
    browser = "Chrome"
    driver = webdriver.Chrome(executable_path=".\\chromedriver")
    try:
        print(f"{browser} Browser opened")
        driver.maximize_window()

        with open("visualizations.txt") as file:
            for visualization in file:
                viz_id = visualization[14:]

                driver.get(f"{URL}{viz_id}")
                print(f"Visualization Name: {driver.title}")
                print(f"Visualization ID: {viz_id}")
                stop = int(input())
                if stop == 1:
                    break

        driver.close()
        print("Closing the session.")
    except Exception as e:
        print(e)
    finally:
        driver.quit()
        print(f"Successfully executed webdriver session for {browser} browser.")
        time.sleep(2)


def generate_random_value(data_type):
    """Return a random value based on given data type.

    :param data_type: Data type to generate random value
    :return: Valid random value based on data type
    """

    if(data_type == 'number'):
        return random.randint(0,100)
    elif(data_type == 'boolean'):
        return bool(random.getrandbits(1))
    elif(data_type == 'date'):
        current_datetime = datetime.datetime.utcnow()
        return str((current_datetime - datetime.timedelta(seconds=random.randint(0,33333333))).isoformat())
    elif(data_type == 'string'):
        return get_random_string(random.randint(3,30))
    else:
        return get_random_string(random.randint(3,30))


def covert_date(elk_date_time):
    try:
        if re.match(r"(^now\/[y|M|w|d|h|H|m|s]$)", elk_date_time):
        
            elk_date_time_unit = re.split(r"^now\/", elk_date_time)[1]
            # print('first if')
            # print(elk_date_time_unit)
            return datetime.datetime.utcnow().isoformat()
        
        elif re.match(r"(^now[-|+]\d+[y|M|w|d|h|H|m|s]$)", elk_date_time):
        
            elk_date_time_unit = re.split(r"(^now)([-|+])(\d+)", elk_date_time)
            current_datetime = datetime.datetime.utcnow()
            # print('second condition')
            # print(current_datetime.isoformat())
            if elk_date_time_unit[2] == '+':
                time = str((current_datetime + datetime.timedelta(seconds=int(elk_date_time_unit[3])*time_mapper[elk_date_time_unit[4]])).isoformat())
            else:
                time = str((current_datetime - datetime.timedelta(seconds=int(elk_date_time_unit[3])*time_mapper[elk_date_time_unit[4]])).isoformat())
            return time
        
        elif re.match(r"(^now([-|+])\d+[y|M|w|d|h|H|m|s]\/[y|M|w|d|h|H|m|s]$)", elk_date_time):
            elk_date_time_unit = re.split(r"(^now)([-|+])(\d+)([y|M|w|d|h|H|m|s])\/([y|M|w|d|h|H|m|s])", elk_date_time)
            # print(elk_date_time_unit)
            if elk_date_time_unit[2] == '+':
                time = str((current_datetime + datetime.timedelta(seconds=int(elk_date_time_unit[3])*time_mapper[elk_date_time_unit[4]])).isoformat())
            else:
                time = str((current_datetime - datetime.timedelta(seconds=int(elk_date_time_unit[3])*time_mapper[elk_date_time_unit[4]])).isoformat())
            return time
        
        elif re.match(r"(^now$)", elk_date_time):
            return datetime.datetime.utcnow().isoformat()
        
        else:
            print('Inside else')
            return False
    except Exception as e:
        print(e)
        return False

def vizualization_query_parser(viz_index):
    valueMapper = {}
    all_query = []
    for hit in visualizations:
        if(hit['_id'] == viz_index):
            source = hit['_source']
            if 'savedSearchId' in source['visualization'] and source['visualization']['savedSearchId'] != '':
                # print('inside')
                SavedSearchId = source['visualization']['savedSearchId']
                # print('putside')
                # print(SavedSearchId)
                for search in searches:
                    if(search['_id'] == 'search:' + SavedSearchId):
                        # print(search)
                        SSJson = search['_source']['search']['kibanaSavedObjectMeta']['searchSourceJSON']
                        # SSJson = json.loads(data)
                        # print(type(data))
            else:
                SSJson = json.loads(source['visualization']['kibanaSavedObjectMeta']['searchSourceJSON'])
            visState = json.loads(source.get('visualization', {}).get('visState', {}))
            aggrs = visState.get('aggs')
            # insideQuery = ''
            for agg in aggrs:
                filters = agg.get('params', {}).get('filters')
                # print(agg)
                # print(filters)
                if filters != None:
                    for filter2 in filters:
                        filter_query = filter2.get('input', {}).get('query', {})
                        all_query.append(filter_query)
                        # insideQuery += filter_query + ' OR '
            
            query = SSJson.get("query").get("query")
            # print(query)
            filter1 = SSJson.get("filter")
            # print(filter1)
            # insideQuery += query
            
            # print(insideQuery)
            indexID = SSJson.get("index")
            # print(indexID)
            if isinstance(query, dict):
                all_query.append(query.get("query_string").get("query"))
                # insideQuery = query.get("query_string").get("query")
            else:
                all_query.append(query)
            # print(insideQuery)
            for insideQuery in all_query:
                insideQuery = re.sub(r'(?!\B(\"|\')[^\"\']*)(?!\\:)(:)(?![^\"\']*(\"|\')\B)', '=', insideQuery)
                insideQuery = re.sub(r'(?!\B(\"|\')[^\"\']*)(?!\\!)(!)(?![^\"\']*(\"|\')\B)', 'NOT ', insideQuery)
                insideQuery = re.sub(r'(?!\B(\"|\')[^\"\']*)( and | \&\& )(?![^\"\']*(\"|\')\B)', ' AND ', insideQuery)
                insideQuery = re.sub(r'(?!\B(\"|\')[^\"\']*)( or | \|\| )(?![^\"\']*(\"|\')\B)', ' OR ', insideQuery)
                insideQuery = insideQuery.replace('(', '')
                insideQuery = insideQuery.replace(')', '')
                queryFields = []
                if insideQuery != '':
                    queryFields = re.split('AND | OR | NOT ', insideQuery)
                # print(queryFields)
                count = 1
                for field in queryFields:
                    keyValue = field.split('=')
                    if(len(keyValue) == 1):
                        valueMapper['freeform' + str(count)] = str(keyValue[0]).rstrip().replace('*', '')
                        # if(isinstance(keyValue[0], str)):
                        #     valueMapper['freeform' + str(count)] = keyValue[0].rstrip().replace('*', '')
                        # else:
                        #     valueMapper['freeform' + str(count)] = keyValue[0]
                        count += 1
                    else:
                        valueMapper[keyValue[0].strip()] = str(keyValue[1]).rstrip().replace('*', '')
                        # if(isinstance(keyValue[1], str)):
                        #     valueMapper[keyValue[0].strip()] = keyValue[1].rstrip().replace('*', '')
                        # else:
                        #     valueMapper[keyValue[0].strip()] = keyValue[1]

            queries = []
            for filter_query in filter1:
                #Check query is active or inactive 
                if filter_query["meta"]["disabled"] == False:
                    # print(filter_query)
                    #query type: is, is not
                    if filter_query.get("query", {}).get("match"):
                        
                        temp_list = list(filter_query["query"]["match"].keys())
                        field_name = temp_list[0]

                        field_value = filter_query["query"]["match"][field_name]["query"]
                
                        # Add for is because is not will be by default due to random value
                        if filter_query["meta"]["negate"] == False:
                            field_name = field_name.replace('.keyword', '')
                            queries.append("{}={}".format(field_name,field_value))
                            valueMapper[field_name] = str(field_value).rstrip().replace('*', '')
                            # if(isinstance(field_value, str)):
                            #     valueMapper[field_name] = field_value.rstrip().replace('*', '')
                            # else:
                            #     valueMapper[field_name] = field_value
                            
                   
                    #query type: is one of, is not one of
                    elif filter_query.get("query", {}).get("bool"):
                        should_elements = []
                        if filter_query["meta"]["negate"] == False:
                            for attr in filter_query["query"]["bool"]["should"]:
                                temp_list = list(attr["match_phrase"].keys())
                                field_name = temp_list[0]
                                field_value = attr["match_phrase"][field_name]
                                should_elements.append(field_value)
                            field_value = should_elements[random.randint(0,len(should_elements)-1)]
                            field_name = field_name.replace('.keyword', '')
                            queries.append("{}={}".format(field_name,field_value))
                            valueMapper[field_name] = str(field_value).rstrip().replace('*', '')
                            # if(isinstance(field_value, str)):
                            #     valueMapper[field_name] = field_value.rstrip().replace('*', '')
                            # else:
                            #     valueMapper[field_name] = field_value

                            
                    # #query type: between, not between
                    elif "range" in filter_query:
                        
                        temp_list = list(filter_query["range"].keys())
                        field_name = temp_list[0]
                    
                        field_lt_value = filter_query["range"][field_name]["lt"]
                        field_gte_value = filter_query["range"][field_name]["gte"]
                        if is_digit(field_lt_value) and is_digit(field_gte_value):
                            if filter_query["meta"]["negate"] == False:
                                field_value = random.randint(field_gte_value, field_lt_value)
                            else:
                                field_value = random.randint(field_lt_value, field_lt_value+100)
                            field_name = field_name.replace('.keyword', '')
                            queries.append("{}={}".format(field_name,field_value))
                            valueMapper[field_name] = str(field_value).rstrip().replace('*', '')
                            # if(isinstance(field_value, str)):
                            #     valueMapper[field_name] = field_value.rstrip().replace('*', '')
                            # else:
                            #     valueMapper[field_name] = field_value
                        
                        else:
                            if filter_query["meta"]["negate"] == False:
                                end_date = covert_date(field_lt_value)
                                if not  end_date:
                                    continue
                                else:
                                    end_date = parser.parse(end_date)
                                    start_date = covert_date(field_gte_value)
                                    if not  start_date:
                                        continue
                                    else:
                                        start_date = parser.parse(start_date)
                                        date3 = start_date + datetime.timedelta(seconds=random.randint(0, int((end_date - start_date).total_seconds())))
                                        field_name = field_name.replace('.keyword', '')
                                        queries.append("{}={}".format(field_name,date3.isoformat()))
                                        valueMapper[field_name] = date3.isoformat()
                    else:
                        print('INSIDE ELSE')
            # for field in queries:
            #     keyValue = field.split('=')
            # valueMapper['OWNERSHIP_TYPE'] = 'SmallBusiness'
            # valueMapper['OWNERSHIP_TYPE'] = '2020-12-20T12:14:05+00:00'
            # valueMapper['freeform2'] = '"messagePlacement=VALIDATE"'
            # valueMapper['freeform3'] = '"DepositsPromotionsResource"'
            # valueMapper['freeform4'] = '"PromotionValidationRequest received"'
            # valueMapper['app_name'] = "token_orchestrator"
            # valueMapper['EVENT'] = "ATTEMPT_3_FAILURE"
            # valueMapper['ClientCorrelationId'] = "EAS"
            # valueMapper['PAYMENT_TRANSACTION_AMOUNT'] = "2555"
            print(valueMapper)
            return indexID, valueMapper

# index = sys.argv[1]
number_of_data = int(sys.argv[1])
# index_pattern_title = sys.argv[3]
FILE_NAME = 'myfile.json'

try:
    with open(FILE_NAME, 'r+') as file_read:
        json_data = json.load(file_read)
    hits = json_data['hits']['hits']
except FileNotFoundError:
    print(FILE_NAME+" not found...")

SEARCH_FILE_NAME = 'saved_search.json'

try:
    with open(SEARCH_FILE_NAME, 'r+') as file_read:
        json_data = json.load(file_read)
    # searches = json_data['hits']['hits']
    searches = json_data
except FileNotFoundError:
    print(SEARCH_FILE_NAME+" not found...")

VIZ_FILE_NAME = 'myfile1.json'
imported_count = 0
skipped_count = 0
available_count = 0
skipped_viz = []
try:
    with open(VIZ_FILE_NAME, 'r+') as file_read:
        json_data_viz = json.load(file_read)
    visualizations = json_data_viz['hits']['hits']
except FileNotFoundError:
    print(VIZ_FILE_NAME+" not found...")
f = open("visualizations.txt", "r")
vizs = f.readlines()
count = 1
for viz in vizs:
    viz = viz.rstrip("\n")
    print("Line{}: {}".format(count, viz.strip()))
    count += 1
    # print(index)
    source = None
    try:
        indexID, valueMapper = vizualization_query_parser(viz)
    except Exception as e:
        print(e)
        print("Error in visualization "+viz)
        continue
    

    index_pattern_title, fieldsData = index_fields(indexID)
    if (fieldsData != None):
        imported_count = 0
        available_count = 0
        skipped_count = 0
        try:
            # fields = json.loads(fieldsData)
            # fields = json.loads(fieldsData.replace('\r\n', ''))
            fields = json.loads(fieldsData, strict=False)
        except Exception as e:
            print(e)
            print("Error in visualization's index pattern "+ viz + ' index-pattern ' + indexID)
            continue
        result = []
        fields_value = []
        fields_type = []
        for field in fields:
            if(not field['name'].startswith('_') and not field['name'].startswith('@') and field['type'] != 'conflict'):
                fields_value.append(field['name'])
                fields_type.append(field['type'])
        for i in range(number_of_data):
            record = {}
            record['@timestamp'] = valueMapper.get('@timestamp' ,generate_random_value('date'))
            freeFromCount = 1
            for j in range(len(fields_value)):
                nested_fields = fields_value[j].split('.')
                if nested_fields[-1] == 'keyword':
                    continue
                # print(fields_value[j])
                # print(nested_fields)
                if len(nested_fields) == 1 and fields_value[j] not in valueMapper and fields_type[j] == 'string':
                    field_random_value = valueMapper.get('freeform' + str(freeFromCount) ,generate_random_value(fields_type[j]))
                    freeFromCount += 1
                else:
                    field_random_value = valueMapper.get(fields_value[j] ,generate_random_value(fields_type[j]))
                for dd in reversed(nested_fields):
                    field_random_value = {dd: field_random_value}
                try:
                    record = update(record, field_random_value)
                except Exception as e:
                    print(e)
                    print("Error in visualization's record update for index pattern "+ viz + ' index-pattern ' + indexID)
                # print(nested_fields)
            result.append(record)
            url = 'http://10.0.8.22:9200/' + index_pattern_title + '/_doc'
            record = json.dumps(record)
            # print(record)
            headers = {'Content-Type': 'application/json'}
            try:
                x = requests.post(url, data = record, headers = headers)
            except requests.ConnectionError as e: 
                print(e)
                print("Error occured while importing data. Visualization ID: {0}".format(viz))
            if(x.status_code == 201):
                imported_count += 1
            elif(x.status_code == 200):
                available_count += 1
            else:
                print(x.json())
                print("Error occured while importing data. Visualization ID: {0} and status code {1}".format(viz, x.status_code))
        # print("Imported {0} indexes, {1} already imported indexes and {2} skipped indexes.".format(imported_count, available_count, skipped_count))
        # print(result)
        # dummy_data = json.dumps(result)
        # file1 = open("dummy_data2.json","w")
        # file1.write(dummy_data)
    else:
        print("Index is not found")
# verify()



def verify():
    """Start of the code."""
    URL = "http://10.0.8.22:5601/app/kibana#/visualize/edit/"
    browser = "Chrome"
    driver = webdriver.Chrome(executable_path=".\\chromedriver_win32\\chromedriver")
    try:
        print(f"{browser} Browser opened")
        driver.maximize_window()

        with open("visualizations.txt") as file:
            for visualization in file:
                viz_id = visualization[14:]

                driver.get(f"{URL}{viz_id}")
                print(f"Visualization Name: {driver.title}")
                print(f"Visualization ID: {viz_id}")
                stop = int(input())
                if stop == 1:
                    break

        driver.close()
        print("Closing the session.")
    except Exception as e:
        print(e)
    finally:
        driver.quit()
        print(f"Successfully executed webdriver session for {browser} browser.")
        time.sleep(2)
