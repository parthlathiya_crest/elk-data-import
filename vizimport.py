import json
import sys
import requests

# Visualization metadata JSON file.
FILE_NAME = 'visualizations valid final.json'
imported_count = 0
skipped_count = 0
available_count = 0
skipped_viz = []
try:
    with open(FILE_NAME, 'r+') as file_read:
        json_data = json.load(file_read)
    hits = json_data['hits']['hits']
except FileNotFoundError:
    print(FILE_NAME+" not found...")
f = open("viz_in.txt", "r")
vizs = f.readlines()
count = 1
for viz in vizs:
    viz = viz.rstrip("\n")
    source = None  
    for hit in hits: 
        if(hit['_id'] == viz):       
        # if(hit['_source']['visualization']['title'] == viz):
            # print(count,':',hit['_source']['visualization']['title'])
            print(hit['_id'])
            count += 1
            source = hit['_source']
            viz_id = hit['_id']
            if 'migrationVersion' in source:
                del source['migrationVersion']
            break
        

    if(source != None):
        dummy_data = json.dumps(source)
        # file1 = open("index_data.json","w")
        # file1.write(dummy_data)
        #prepare payload to send request
        url = 'http://10.0.8.22:9200/' + '.kibana/_doc/' + viz_id
        headers = {'Content-Type': 'application/json'}
        try:
            x = requests.post(url, data = dummy_data, headers = headers)
        except requests.ConnectionError as e: 
            print(e)
        if(x.status_code == 201):
            imported_count += 1
        elif(x.status_code == 200):
            available_count += 1
        else:
            print("Error occured while importing visualization. visualization ID: {0} and status code {1}".format(viz_id, x.status_code))
    else:
        print("None")
        skipped_count += 1
        skipped_viz.append(viz_id)

print("Imported {0} visulizations, {1} already imported visulizations and {2} skipped visulizations.".format(imported_count, available_count, skipped_count))
print("skipped visulizations: ",skipped_viz)
